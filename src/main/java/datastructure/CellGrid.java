package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    private int numRows;
    private int numColumns;
    private CellState[][] cellGrid = new CellState[numRows][numColumns];

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.numRows = rows;
        this.numColumns = columns;
        this.cellGrid = new CellState[numRows][numColumns];
        //this.cellGrid = new CellState[numRows][numColumns];
        for (int r=0; r<rows; r++) {
            for (int c=0; c<columns; c++) {
                this.set(r, c, initialState);
            }
        }
	}

    @Override
    public int numRows() {
        return this.numRows;
    }

    @Override
    public int numColumns() {
        return this.numColumns;
    }

    static class IndexOutOfBoundsException extends Exception {
        public IndexOutOfBoundsException(String errorMessage) {
            super(errorMessage);
        }
    }

    private void checkInBounds(int index, int maxIndex) throws IndexOutOfBoundsException{
        if (index >= maxIndex) {
            throw new IndexOutOfBoundsException("CellGrid index out of bounds"); 
        }
    }

    @Override
    public void set(int row, int column, CellState element) {
        try {
            this.checkInBounds(row, this.numRows());
        } catch (IndexOutOfBoundsException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            this.checkInBounds(column, this.numColumns());
        } catch (IndexOutOfBoundsException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        this.cellGrid[row][column] = element;        
    }

    @Override
    public CellState get(int row, int column) {
        try {
            this.checkInBounds(row, this.numRows());
        } catch (IndexOutOfBoundsException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            this.checkInBounds(column, this.numColumns());
        } catch (IndexOutOfBoundsException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return this.cellGrid[row][column];
    }

    @Override
    public IGrid copy() {
        IGrid grid = new CellGrid(this.numRows(), this.numColumns(), CellState.DEAD);
        for (int r=0; r<this.numRows(); r++) {
            for (int c=0; c<this.numColumns(); c++) {
                grid.set(r, c, this.get(r,c));
            }
        }
        return grid;
    }
    
}
